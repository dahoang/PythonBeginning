s='Hi John, welcome to python programming for beginner!'
'python'in s
print('python in s','python'in s)
print('python not in s','python'not in s)
a,b=s.split(",")
#tách chuỗi s thành 2 đoạn a b, loại bỏ dấu , cạnh chữ John
k=a.split(" ") # tách tiếp chuỗi a thành 2 đoạn
print('k=',a.split(" ")) # tách chuỗi a thành Hi - John
s1=k[1]
print('s1=',k[1]) # gán biến s1 = giá trị thứ 1 của chuỗi k
print('len of s=',len(s)) #đếm số ký tự của 1 chuỗi
print('count many work in s=',s.count(" ")+1)
# đếm số chữ trong 1 chuỗi, = tổng khoảng trắng +1
#output kết quả
#python in s True
#python not in s False
#k= ['Hi', 'John']
#s1= John
#len of s= 52
#count many work in s= 8
