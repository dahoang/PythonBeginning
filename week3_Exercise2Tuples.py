# coding: utf-8

# In[8]:


t=(1,'python',[2,3],(4,5))
x1,x2,[x3,x4],[x5,x6]=t
print(t[-1]) #Print out the last element of t


# In[45]:


#Add to t a list [2, 3]
t=(1,'python',[2,3],(4,5))
t1=([2,3],)
t=t1+t
t
t=list(t) #chuyển t thành type list để sử dụng hàm count, remove
#Check whether list [2, 3] is duplicated in t
t.count([2,3])>1
# nếu t.count([2,3])>1 thì [2,3] duplicate trong t
print('[2,3] is duplicate in t=',t.count([2,3])>1)
#Remove list [2, 3] from t
t.remove([2,3])
t=tuple(t) #trả lại type của t là tuple
#Convert tuple t into a list
t=list(t)
type(t)

