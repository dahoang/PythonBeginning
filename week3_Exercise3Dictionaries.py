
# coding: utf-8

# In[21]:


#3.1 Write a Python script to concatenate following dictionaries to create a new one.
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
dic={}
dic.update(dic1)
dic.update(dic2)
dic.update(dic3)
dic # Expected result: {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
dict={} #create a dict
for x in range (1,16): #vì đếm bắt đầu từ 0 nên khoảng (1,16)
    dict[x]=x**2 #value của x = x bình phương
print(dict)


# In[84]:


#3.2 sort ascending a dict by value
dict1={'a': 1, 'b': 4, 'c': 2} 
z=list(dict1.items()) #chuyển thành list
z.sort(key=lambda x:x[1]) # sort using 2 item 
z1=[z[0] for z in z]
z1


# In[140]:


#3.3
content = 'Python is an easy language to learn'
d = {}
for x in content:
    if x not in d:
        d[x] = 1
    else:
        d[x] = d[x] + 1
print (d)

