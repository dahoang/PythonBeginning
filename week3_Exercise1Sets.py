# coding: utf-8

# In[13]:


A = {1, 2, 3, 4, 5, 7} #create set A
B = {2, 4, 5, 9, 12, 24} #create set B
C = {2, 4, 8} #create set C
print(C) #Iterate over all elements of set C


# In[15]:


print('A|C=',A|C)
print('B|C=',B|C)
#Print out A and B after adding elements
#A|C add each element to A
#B|C add each element to B


# In[17]:


#Print out the intersection of A and B
print('A&B=',A&B)


# In[18]:


#Print out the union of A and B
print('A|B=',A|B)


# In[19]:


#Print out elements in A but not in B
print('A-B=',A-B)


# In[20]:


#Print out the length of A and B
print('len(A)=',len(A))
print('len(B)=',len(B))


# In[22]:


#Print out the maximum value of A union B
print('max A union B=',max(A|B))


# In[23]:


#Print out the minimum value of A union B
print('min A union B=',min(A|B))

