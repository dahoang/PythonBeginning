I=[23,4.3,4.2,31,'python',1,5.3,9,1.7]
I.remove('python') #xóa phần tử python trong I
print(I)
I.sort()#sắp xếp tăng dần
print(I)
I.reverse() #sắp xếp giảm dần
print(I)
t=4.2 not in I # kiểm tra 4.2 có trong I hay không
print('4.2 not in I is',t)
#output
#[23, 4.3, 4.2, 31, 1, 5.3, 9, 1.7]
#[1, 1.7, 4.2, 4.3, 5.3, 9, 23, 31]
#[31, 23, 9, 5.3, 4.3, 4.2, 1.7, 1]
#4.2 not in I is False